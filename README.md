# Lunch Recipes

Flutter app for both iOS and Android.

> Mobile Technical Task: Apps for Lunch Recipes Suggestion

**Source Code: [Bitbucket](https://bitbucket.org/jeffryhermanto/hermanto-jeffry-techtask-mobile/src/master/)**

## Screenshots

![Screenshots](https://firebasestorage.googleapis.com/v0/b/jh-project-b7418.appspot.com/o/lunch_recipes%2Fscreenshots.png?alt=media&token=8e7b1ac8-323b-4aa0-a841-4db7ffeb48bc)

## Architecture

Flutter TDD Clean Architecture

![Flutter TDD Clean Architecture](https://firebasestorage.googleapis.com/v0/b/jh-project-b7418.appspot.com/o/lunch_recipes%2FClean-Architecture-Flutter-Diagram.png?alt=media&token=015c22c6-238f-443b-8389-62de091b6820)

## Flutter Documentation

For help with Flutter, view
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Copy Repository Locally

Enter git clone and the repository URL at your command line:

`$ git clone https://jeffryhermanto@bitbucket.org/jeffryhermanto/hermanto-jeffry-techtask-mobile.git`

## Running The App

To launch the app in the Simulator, ensure that the Simulator is running and enter the following command from the root of the project in the terminal:

- `$ flutter pub get`
- `$ flutter run`

## Run Tests Using VSCode

The Flutter plugins for VSCode support running tests. This is often the best option while writing tests because it provides the fastest feedback loop as well as the ability to set breakpoints.

1. Select the Debug menu
2. Click the Start Debugging option

---

**Developed by [Jeffry Hermanto](http://www.jhproject.id)**

+62 821 7777 1078  
_jeffryhermanto@gmail.com_  
www.jhproject.id

[Facebook](https://www.facebook.com/jeffryhermanto) |
[Instagram](https://www.instagram.com/jeffryhermanto) |
[Twitter](https://www.twitter.com/jeffryhermanto) |
[LinkedIn](https://www.linkedin.com/in/jeffryhermanto) |
[YouTube](https://www.youtube.com/jeffryhermanto) |
[GitHub](https://github.com/jeffryhermanto)

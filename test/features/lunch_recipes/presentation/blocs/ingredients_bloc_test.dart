import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:mockito/mockito.dart';

import 'package:lunch_recipes/features/lunch_recipes/presentation/bloc/bloc.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/usecases/get_ingredients.dart';

class MockGetIngredients extends Mock implements GetIngredients {}

void main() {
  MockGetIngredients getIngredients;

  setUp(() {
    getIngredients = MockGetIngredients();
  });

  group('Ingredients BLoC Test', () {
    blocTest(
      'should return Initial Value',
      build: () => IngredientsBloc(getIngredients: getIngredients),
      expect: [InitialIngredientsState()],
    );
    blocTest(
      'should return IngredientsLoading state',
      build: () => IngredientsBloc(getIngredients: getIngredients),
      act: (bloc) async {
        bloc.add(LoadIngredients());
      },
      expect: [
        InitialIngredientsState(),
        IngredientsLoading(),
      ],
    );
  });
}

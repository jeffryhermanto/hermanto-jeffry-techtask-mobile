import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:mockito/mockito.dart';

import 'package:lunch_recipes/features/lunch_recipes/presentation/bloc/bloc.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/usecases/get_recipes.dart';
import 'package:lunch_recipes/core/util/constants.dart';

class MockGetRecipes extends Mock implements GetRecipes {}

void main() {
  MockGetRecipes getRecipes;

  setUp(() {
    getRecipes = MockGetRecipes();
  });

  final tApiURL = kMockBaseURL + '/recipes?ingredients=<title-1>,<title-n>';
  group('Recipes BLoC Test', () {
    blocTest(
      'should return Initial Value',
      build: () => RecipesBloc(getRecipes: getRecipes),
      expect: [InitialRecipesState()],
    );
    blocTest(
      'should return RecipesLoading state',
      build: () => RecipesBloc(getRecipes: getRecipes),
      act: (bloc) async {
        bloc.add(LoadRecipes(apiURL: tApiURL));
      },
      expect: [
        InitialRecipesState(),
        RecipesLoading(),
      ],
    );
  });
}

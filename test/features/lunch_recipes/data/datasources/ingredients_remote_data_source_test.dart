import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;
import 'package:matcher/matcher.dart';

import 'package:lunch_recipes/features/lunch_recipes/data/datasources/ingredients_remote_data_source.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/models/ingredient_model.dart';
import 'package:lunch_recipes/core/util/constants.dart';
import 'package:lunch_recipes/core/error/exceptions.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  IngredientsRemoteDataSourceImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = IngredientsRemoteDataSourceImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => http.Response(fixture('ingredients.json'), 200));
  }

  void setUpMockHttpClientFailure404() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response('Something went wrong', 404));
  }

  group('getIngredients', () {
    final tApiURL = kMockBaseURL + '/ingredients';
    test(
      'should perform a GET request on a URL and with application/json header',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        dataSource.getIngredients();
        // assert
        verify(mockHttpClient.get(
          tApiURL,
          headers: {
            'Content-Type': 'application/json',
          },
        ));
      },
    );

    test(
      'should return List of Ingredient when the response code is 200 (success)',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final result = await dataSource.getIngredients();
        // assert
        expect(result, equals(isA<List<IngredientModel>>()));
      },
    );

    test(
        'should throw a ServerException when the response code is 404 or other',
        () async {
      // arrange
      setUpMockHttpClientFailure404();
      // act
      final call = dataSource.getIngredients;
      // assert
      expect(() => call(), throwsA(TypeMatcher<ServerException>()));
    });
  });
}

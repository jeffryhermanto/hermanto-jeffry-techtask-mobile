import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;
import 'package:matcher/matcher.dart';

import 'package:lunch_recipes/features/lunch_recipes/data/datasources/recipes_remote_data_source.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/models/recipe_model.dart';
import 'package:lunch_recipes/core/util/constants.dart';
import 'package:lunch_recipes/core/error/exceptions.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  RecipesRemoteDataSourceImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = RecipesRemoteDataSourceImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response(fixture('recipes.json'), 200));
  }

  void setUpMockHttpClientFailure404() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response('Something went wrong', 404));
  }

  group('getRecipes', () {
    final tApiURL = kMockBaseURL + '/recipes?ingredients=<title-1>,<title-n>';
    final tRecipeModels = [
      RecipeModel(title: 'Ham and Cheese Toastie', ingredients: [
        'Ham',
        'Cheese',
        'Bread',
        'Butter',
      ]),
      RecipeModel(title: 'Salad', ingredients: [
        'Lettuce',
        'Tomato',
        'Cucumber',
        'Beetroot',
        'Salad Dressing'
      ]),
      RecipeModel(title: 'Hotdog', ingredients: [
        'Hotdog Bun',
        'Sausage',
        'Ketchup',
        'Mustard',
      ]),
    ];

    test(
      'should perform a GET request on a URL with ingredients being the endpoint and with application/json header',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        dataSource.getRecipes(tApiURL);
        // assert
        verify(mockHttpClient.get(
          tApiURL,
          headers: {
            'Content-Type': 'application/json',
          },
        ));
      },
    );

    test(
      'should return List of Ingredient when the response code is 200 (success)',
      () async {
        // arrange
        setUpMockHttpClientSuccess200();
        // act
        final result = await dataSource.getRecipes(tApiURL);
        // assert
        expect(result, equals(tRecipeModels));
      },
    );

    test(
        'should throw a ServerException when the response code is 404 or other',
        () async {
      // arrange
      setUpMockHttpClientFailure404();
      // act
      final call = dataSource.getRecipes;
      // assert
      expect(() => call(tApiURL), throwsA(TypeMatcher<ServerException>()));
    });
  });
}

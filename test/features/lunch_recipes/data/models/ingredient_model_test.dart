import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/models/ingredient_model.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/entities/ingredient.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tIngredientModel = IngredientModel(
    title: 'Ham and Cheese Toastie',
    useBy: '2020-11-25',
  );

  group('Ingredient Model', () {
    test('should be a subclass of Ingredient entity', () async {
      expect(tIngredientModel, isA<Ingredient>());
    });

    test(
      'should return a valid model from JSON',
      () async {
        // arrange
        final List<dynamic> jsonMaps = json.decode(fixture('ingredients.json'));
        // act
        final result =
            IngredientModel.fromJson(jsonMaps[0] as Map<String, dynamic>);
        // assert
        expect(result, isA<IngredientModel>());
      },
    );
  });
}

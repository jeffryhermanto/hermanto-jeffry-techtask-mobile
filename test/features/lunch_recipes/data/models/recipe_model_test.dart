import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/models/recipe_model.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/entities/recipe.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tRecipeModel = RecipeModel(
    title: 'Ham and Cheese Toastie',
    ingredients: ['Ham', 'Cheese', 'Bread', 'Butter'],
  );

  group('Recipe Model', () {
    test('should be a subclass of Recipe entity', () async {
      expect(tRecipeModel, isA<Recipe>());
    });

    test(
      'should return a valid model from JSON',
      () async {
        // arrange
        final List<dynamic> jsonMaps = json.decode(fixture('recipes.json'));
        // act
        final result =
            RecipeModel.fromJson(jsonMaps[0] as Map<String, dynamic>);
        // assert
        expect(result, tRecipeModel);
      },
    );
  });
}

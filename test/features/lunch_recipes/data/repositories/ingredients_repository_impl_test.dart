import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:dartz/dartz.dart';

import 'package:lunch_recipes/features/lunch_recipes/data/datasources/ingredients_remote_data_source.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/repositories/ingredients_repository_impl.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/models/ingredient_model.dart';
import 'package:lunch_recipes/core/network/network_info.dart';
import 'package:lunch_recipes/core/error/exceptions.dart';
import 'package:lunch_recipes/core/error/failures.dart';

class MockRemoteDataSource extends Mock implements IngredientsRemoteDataSource {
}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  IngredientsRepositoryImpl repository;
  MockRemoteDataSource mockRemoteDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = IngredientsRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  void runTestsOffline(Function body) {
    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      body();
    });
  }

  group('Get List of Ingredient Models', () {
    final List<IngredientModel> tIngredientModels = [
      IngredientModel(title: 'Ham', useBy: '2020-11-25'),
      IngredientModel(title: 'Cheese', useBy: '2020-01-08'),
      IngredientModel(title: 'Bread', useBy: '2020-11-01'),
      IngredientModel(title: 'Butter', useBy: '2020-11-25'),
      IngredientModel(title: 'Bacon', useBy: '2020-11-02'),
      IngredientModel(title: 'Eggs', useBy: '2020-11-25'),
      IngredientModel(title: 'Mushrooms', useBy: '2020-11-11'),
      IngredientModel(title: 'Sausage', useBy: '2020-11-25'),
      IngredientModel(title: 'Hotdog Bun', useBy: '2019-11-25'),
      IngredientModel(title: 'Ketchup', useBy: '2019-11-11'),
      IngredientModel(title: 'Mustard', useBy: '2019-11-10'),
      IngredientModel(title: 'Lettuce', useBy: '2019-11-10'),
      IngredientModel(title: 'Tomato', useBy: '2019-11-05'),
      IngredientModel(title: 'Cucumber', useBy: '2019-11-05'),
      IngredientModel(title: 'Beetroot', useBy: '2019-11-06'),
      IngredientModel(title: 'Salad Dressing', useBy: '2019-11-06'),
    ];
    test('should check if the device is online', () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      // act
      repository.getIngredients();
      // assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestsOnline(() {
      test(
          'should return remote data when the call to remote data source is successful',
          () async {
        // arrange
        when(mockRemoteDataSource.getIngredients())
            .thenAnswer((_) async => tIngredientModels);
        // act
        final result = await repository.getIngredients();
        // assert
        verify(mockRemoteDataSource.getIngredients());
        expect(result, equals(Right(tIngredientModels)));
      });
    });

    runTestsOffline(() {
      test('should return Server Failure when there is no connection',
          () async {
        // arrange
        when(mockRemoteDataSource.getIngredients())
            .thenThrow(ServerException());
        // act
        final result = await repository.getIngredients();
        // assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(Left(ServerFailure(message: 'No Network'))));
      });
    });
  });
}

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:dartz/dartz.dart';

import 'package:lunch_recipes/features/lunch_recipes/data/datasources/recipes_remote_data_source.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/repositories/recipes_repository_impl.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/models/recipe_model.dart';
import 'package:lunch_recipes/core/network/network_info.dart';
import 'package:lunch_recipes/core/error/exceptions.dart';
import 'package:lunch_recipes/core/error/failures.dart';
import 'package:lunch_recipes/core/util/constants.dart';

class MockRemoteDataSource extends Mock implements RecipesRemoteDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  RecipesRepositoryImpl repository;
  MockRemoteDataSource mockRemoteDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = RecipesRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  void runTestsOffline(Function body) {
    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      body();
    });
  }

  group('Get List of Recipe Models', () {
    final tApiURL = kMockBaseURL + '/recipes?ingredients=<title-1>,<title-n>';
    final List<RecipeModel> tRecipeModels = [
      RecipeModel(title: 'Ham and Cheese Toastie', ingredients: [
        'Ham',
        'Cheese',
        'Bread',
        'Butter',
      ]),
      RecipeModel(title: 'Salad', ingredients: [
        'Lettuce',
        'Tomato',
        'Cucumber',
        'Beetroot',
        'Salad Dressing'
      ]),
      RecipeModel(title: 'Hotdog', ingredients: [
        'Hotdog Bun',
        'Sausage',
        'Ketchup',
        'Mustard',
      ]),
    ];
    test('should check if the device is online', () async {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      // act
      repository.getRecipes(tApiURL);
      // assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestsOnline(() {
      test(
          'should return remote data when the call to remote data source is successful',
          () async {
        // arrange
        when(mockRemoteDataSource.getRecipes(tApiURL))
            .thenAnswer((_) async => tRecipeModels);
        // act
        final result = await repository.getRecipes(tApiURL);
        // assert
        verify(mockRemoteDataSource.getRecipes(tApiURL));
        expect(result, equals(Right(tRecipeModels)));
      });
    });

    runTestsOffline(() {
      test('should return Server Failure when there is no connection',
          () async {
        // arrange
        when(mockRemoteDataSource.getRecipes(tApiURL))
            .thenThrow(ServerException());
        // act
        final result = await repository.getRecipes(tApiURL);
        // assert
        verifyZeroInteractions(mockRemoteDataSource);
        expect(result, equals(Left(ServerFailure(message: 'No Network'))));
      });
    });
  });
}

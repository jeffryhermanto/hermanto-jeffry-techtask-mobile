import 'package:flutter_test/flutter_test.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/models/ingredient_model.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/entities/ingredient.dart';

void main() {
  final tTitle = 'Ham and Cheese Toastie';
  final tUseBy = '2020-11-25';
  final tIngredient = Ingredient(
    title: tTitle,
    useBy: tUseBy,
  );

  group('Ingredient Entity', () {
    test('should be a valid Ingredient object', () {
      expect(tIngredient.title, equals(tTitle));
      expect(tIngredient.useBy, equals(tUseBy));
    });

    test('should return an Ingredient Model', () {
      final result = tIngredient.toModel();
      expect(result, isA<IngredientModel>());
    });
  });
}

import 'package:flutter_test/flutter_test.dart';
import 'package:lunch_recipes/features/lunch_recipes/data/models/recipe_model.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/entities/recipe.dart';

void main() {
  final tTitle = 'Ham and Cheese Toastie';
  final tIngredients = ['Ham', 'Cheese', 'Bread', 'Butter'];
  final tRecipe = Recipe(
    title: tTitle,
    ingredients: tIngredients,
  );

  group('Recipe Entity', () {
    test('should be a valid Recipe object', () {
      expect(tRecipe.title, equals(tTitle));
      expect(tRecipe.ingredients, equals(tIngredients));
    });

    test('should return a Recipe Model', () {
      final result = tRecipe.toModel();
      expect(result, isA<RecipeModel>());
    });
  });
}

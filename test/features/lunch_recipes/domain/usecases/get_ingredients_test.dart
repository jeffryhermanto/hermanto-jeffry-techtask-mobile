import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:dartz/dartz.dart';

import 'package:lunch_recipes/core/usecases/usecase.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/usecases/get_ingredients.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/repositories/ingredients_repository.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/entities/ingredient.dart';

class MockIngredientsRepository extends Mock implements IngredientsRepository {}

void main() {
  GetIngredients usecase;
  MockIngredientsRepository mockIngredientsRepository;

  setUp(() {
    mockIngredientsRepository = MockIngredientsRepository();
    usecase = GetIngredients(mockIngredientsRepository);
  });

  final tIngredients = [
    Ingredient(title: 'Ham', useBy: '2020-11-25'),
    Ingredient(title: 'Cheese', useBy: '2020-01-08'),
    Ingredient(title: 'Bread', useBy: '2020-11-01'),
    Ingredient(title: 'Butter', useBy: '2020-11-25'),
    Ingredient(title: 'Bacon', useBy: '2020-11-02'),
    Ingredient(title: 'Eggs', useBy: '2020-11-25'),
    Ingredient(title: 'Mushrooms', useBy: '2020-11-11'),
    Ingredient(title: 'Sausage', useBy: '2020-11-25'),
    Ingredient(title: 'Hotdog Bun', useBy: '2019-11-25'),
    Ingredient(title: 'Ketchup', useBy: '2019-11-11'),
    Ingredient(title: 'Mustard', useBy: '2019-11-10'),
    Ingredient(title: 'Lettuce', useBy: '2019-11-10'),
    Ingredient(title: 'Tomato', useBy: '2019-11-05'),
    Ingredient(title: 'Cucumber', useBy: '2019-11-05'),
    Ingredient(title: 'Beetroot', useBy: '2019-11-06'),
    Ingredient(title: 'Salad Dressing', useBy: '2019-11-06'),
  ];

  test(
    'should get List of Ingredient from the repository',
    () async {
      // arrange
      when(mockIngredientsRepository.getIngredients())
          .thenAnswer((_) async => Right(tIngredients));
      // act
      final result = await usecase(NoParams());
      // assert
      expect(result, Right(tIngredients));
      verify(mockIngredientsRepository.getIngredients());
      verifyNoMoreInteractions(mockIngredientsRepository);
    },
  );
}

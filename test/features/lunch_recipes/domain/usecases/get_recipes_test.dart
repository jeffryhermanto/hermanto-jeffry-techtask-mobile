import 'package:flutter_test/flutter_test.dart';
import 'package:lunch_recipes/core/util/constants.dart';
import 'package:mockito/mockito.dart';
import 'package:dartz/dartz.dart';

import 'package:lunch_recipes/features/lunch_recipes/domain/usecases/get_recipes.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/repositories/recipes_repository.dart';
import 'package:lunch_recipes/features/lunch_recipes/domain/entities/recipe.dart';

class MockRecipesRepository extends Mock implements RecipesRepository {}

void main() {
  GetRecipes usecase;
  MockRecipesRepository mockRecipesRepository;

  setUp(() {
    mockRecipesRepository = MockRecipesRepository();
    usecase = GetRecipes(mockRecipesRepository);
  });

  final tApiURL = kMockBaseURL + '/recipes?ingredients=<title-1>,<title-n>';
  final tRecipes = [
    Recipe(title: 'Ham and Cheese Toastie', ingredients: [
      'Ham',
      'Cheese',
      'Bread',
      'Butter',
    ]),
    Recipe(title: 'Salad', ingredients: [
      'Lettuce',
      'Tomato',
      'Cucumber',
      'Beetroot',
      'Salad Dressing'
    ]),
    Recipe(title: 'Hotdog', ingredients: [
      'Hotdog Bun',
      'Sausage',
      'Ketchup',
      'Mustard',
    ]),
  ];

  test(
    'should get List of Recipe from the repository',
    () async {
      // arrange
      when(mockRecipesRepository.getRecipes(tApiURL))
          .thenAnswer((_) async => Right(tRecipes));
      // act
      final result = await usecase(RecipeParams(apiURL: tApiURL));
      // assert
      expect(result, Right(tRecipes));
      verify(mockRecipesRepository.getRecipes(tApiURL));
      verifyNoMoreInteractions(mockRecipesRepository);
    },
  );
}

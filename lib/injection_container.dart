import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import 'core/network/network_info.dart';
import 'features/lunch_recipes/data/datasources/ingredients_remote_data_source.dart';
import 'features/lunch_recipes/data/datasources/recipes_remote_data_source.dart';
import 'features/lunch_recipes/data/repositories/ingredients_repository_impl.dart';
import 'features/lunch_recipes/data/repositories/recipes_repository_impl.dart';
import 'features/lunch_recipes/domain/repositories/ingredients_repository.dart';
import 'features/lunch_recipes/domain/repositories/recipes_repository.dart';
import 'features/lunch_recipes/domain/usecases/usecases.dart';
import 'features/lunch_recipes/presentation/bloc/bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
  registerLunchRecipesFeatures();
  await registerCoreAndExternalDependencies();
  return null;
}

void registerLunchRecipesFeatures() {
  //! Features - Lunch Recipes
  // BLoC
  sl.registerFactory(
    () => IngredientsBloc(
      getIngredients: sl(),
    ),
  );

  sl.registerFactory(
    () => RecipesBloc(
      getRecipes: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => GetIngredients(sl()));
  sl.registerLazySingleton(() => GetRecipes(sl()));

  // Repository
  sl.registerLazySingleton<IngredientsRepository>(
    () => IngredientsRepositoryImpl(
      remoteDataSource: sl(),
      networkInfo: sl(),
    ),
  );

  sl.registerLazySingleton<RecipesRepository>(
    () => RecipesRepositoryImpl(
      remoteDataSource: sl(),
      networkInfo: sl(),
    ),
  );

  // Data sources
  sl.registerLazySingleton<IngredientsRemoteDataSource>(
    () => IngredientsRemoteDataSourceImpl(client: http.Client()),
  );

  sl.registerLazySingleton<RecipesRemoteDataSource>(
    () => RecipesRemoteDataSourceImpl(client: http.Client()),
  );
}

Future<void> registerCoreAndExternalDependencies() async {
  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //! External
  sl.registerLazySingleton(() => DataConnectionChecker());
}

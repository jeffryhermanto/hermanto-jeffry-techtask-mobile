import 'package:flutter/material.dart';

// Colors
const kLoadsmileColor = Color(0xffE9462F);

// API URL
const String kMockBaseURL =
    'https://lb7u7svcm5.execute-api.ap-southeast-1.amazonaws.com/dev';

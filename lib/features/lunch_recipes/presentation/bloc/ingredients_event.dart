import 'package:equatable/equatable.dart';

abstract class IngredientsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadIngredients extends IngredientsEvent {
  @override
  String toString() => 'Load Ingredients';
}

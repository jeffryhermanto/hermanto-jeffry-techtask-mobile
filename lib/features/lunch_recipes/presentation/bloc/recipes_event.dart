import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RecipesEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadRecipes extends RecipesEvent {
  final String apiURL;

  LoadRecipes({@required this.apiURL});

  @override
  String toString() => 'Load Recipes';
}

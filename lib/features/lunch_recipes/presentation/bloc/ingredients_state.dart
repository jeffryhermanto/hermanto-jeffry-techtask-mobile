import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../domain/entities/ingredient.dart';

abstract class IngredientsState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialIngredientsState extends IngredientsState {}

class IngredientsLoading extends IngredientsState {
  @override
  String toString() => 'Ingredients Loading';
}

class IngredientsLoaded extends IngredientsState {
  final List<Ingredient> ingredients;

  IngredientsLoaded({@required this.ingredients});

  @override
  String toString() => 'Ingredients Loaded { Ingredients: $ingredients }';

  @override
  List<Object> get props => [ingredients];
}

class IngredientsError extends IngredientsState {
  final String message;

  IngredientsError({@required this.message});

  @override
  String toString() => 'Ingredients Error { message: $message }';
}

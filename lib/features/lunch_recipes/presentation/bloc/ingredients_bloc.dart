import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import './bloc.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../domain/usecases/get_ingredients.dart';

class IngredientsBloc extends Bloc<IngredientsEvent, IngredientsState> {
  GetIngredients _getIngredients;

  IngredientsBloc({
    @required GetIngredients getIngredients,
  })  : assert(getIngredients != null),
        _getIngredients = getIngredients;

  @override
  IngredientsState get initialState => InitialIngredientsState();

  @override
  Stream<IngredientsState> mapEventToState(
    IngredientsEvent event,
  ) async* {
    if (event is LoadIngredients) {
      yield* _mapLoadIngredientsToState(_getIngredients);
    }
  }

  Stream<IngredientsState> _mapLoadIngredientsToState(
    GetIngredients getIngredients,
  ) async* {
    yield IngredientsLoading();

    final failureOrTags = await getIngredients(NoParams());

    yield failureOrTags.fold(
      (failure) {
        return IngredientsError(message: _mapFailureToMessage(failure));
      },
      (ingredients) {
        return IngredientsLoaded(ingredients: ingredients);
      },
    );
  }
}

String _mapFailureToMessage(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return failure.props[0].toString();
    case CacheFailure:
      return 'Cache Failure';
    default:
      return 'Unexpected error';
  }
}

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import './bloc.dart';
import '../../../../core/error/failures.dart';
import '../../domain/usecases/usecases.dart';

class RecipesBloc extends Bloc<RecipesEvent, RecipesState> {
  GetRecipes _getRecipes;

  RecipesBloc({
    @required GetRecipes getRecipes,
  })  : assert(getRecipes != null),
        _getRecipes = getRecipes;

  @override
  RecipesState get initialState => InitialRecipesState();

  @override
  Stream<RecipesState> mapEventToState(
    RecipesEvent event,
  ) async* {
    if (event is LoadRecipes) {
      yield* _mapLoadRecipesToState(_getRecipes, event.apiURL);
    }
  }

  Stream<RecipesState> _mapLoadRecipesToState(
    GetRecipes getRecipes,
    String apiURL,
  ) async* {
    yield RecipesLoading();

    final failureOrTags = await getRecipes(RecipeParams(apiURL: apiURL));

    yield failureOrTags.fold(
      (failure) {
        return RecipesError(message: _mapFailureToMessage(failure));
      },
      (recipes) {
        return RecipesLoaded(recipes: recipes);
      },
    );
  }
}

String _mapFailureToMessage(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return failure.props[0].toString();
    case CacheFailure:
      return 'Cache Failure';
    default:
      return 'Unexpected error';
  }
}

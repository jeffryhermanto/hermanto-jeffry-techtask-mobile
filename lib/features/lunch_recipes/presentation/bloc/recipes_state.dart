import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../domain/entities/recipe.dart';

abstract class RecipesState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialRecipesState extends RecipesState {}

class RecipesLoading extends RecipesState {
  @override
  String toString() => 'Recipes Loading';
}

class RecipesLoaded extends RecipesState {
  final List<Recipe> recipes;

  RecipesLoaded({@required this.recipes});

  @override
  String toString() => 'Recipes Loaded { Recipes: $recipes }';

  @override
  List<Object> get props => [recipes];
}

class RecipesError extends RecipesState {
  final String message;

  RecipesError({@required this.message});

  @override
  String toString() => 'Recipes Error { message: $message }';
}

import 'package:flutter/material.dart';

import '../../../../core/util/constants.dart';
import '../../domain/entities/ingredient.dart';
import '../screens/recipes_screen.dart';

class GetRecipesButton extends StatelessWidget {
  final List<Ingredient> selectedIngredients;

  GetRecipesButton(this.selectedIngredients);

  //? [START Helper Methods]
  void getRecipes(BuildContext context) {
    String apiURL = kMockBaseURL + '/recipes?ingredients=';

    for (int i = 0; i < selectedIngredients.length; i++) {
      if (i == selectedIngredients.length - 1) {
        apiURL += selectedIngredients[i].title;
      } else {
        apiURL += selectedIngredients[i].title + ',';
      }
    }

    print('API URL: $apiURL');

    _navigateToRecipesScreen(context, apiURL);
  }

  void _navigateToRecipesScreen(BuildContext context, String apiURL) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RecipesScreen(apiURL, selectedIngredients),
      ),
    );
  }
  //? [END Helper Methods]

  //! [START Screen Build]
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0),
      child: RaisedButton(
        padding: EdgeInsets.all(15.0),
        color: kLoadsmileColor,
        child: Text(
          'Get Recipes',
          style: TextStyle(color: Colors.white),
        ),
        onPressed: (selectedIngredients.length == 0)
            ? null
            : () => getRecipes(context),
      ),
    );
  }
  //! [START Screen Build]
}

import 'package:flutter/material.dart';

import '../../../../core/util/constants.dart';
import '../../domain/entities/ingredient.dart';

class IngredientList extends StatelessWidget {
  final List<Ingredient> ingredients;
  final Function onPressed;

  const IngredientList({
    @required this.ingredients,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0),
      child: ingredients.length == 0
          ? Text(
              'Sorry... All ingredients expired.',
              style: TextStyle(
                color: Colors.red,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.bold,
              ),
            )
          : Wrap(
              spacing: 8.0, // gap between adjacent chips
              runSpacing: 0.0, // gap between lines
              children: ingredients.map((ingredient) {
                return ActionChip(
                  label: Text(ingredient.title),
                  labelStyle: TextStyle(
                    color: ingredient.selected ? Colors.white : Colors.black,
                  ),
                  backgroundColor:
                      ingredient.selected ? kLoadsmileColor : Colors.black12,
                  onPressed: () => onPressed(ingredient),
                );
              }).toList(),
            ),
    );
  }
}

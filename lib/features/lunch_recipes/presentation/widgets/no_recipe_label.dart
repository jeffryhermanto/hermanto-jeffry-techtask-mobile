import 'package:flutter/material.dart';

class NoRecipeLabel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24.0),
      child: Text(
        'Sorry... No Recipe for these ingredients.',
        style: TextStyle(
          color: Colors.red,
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

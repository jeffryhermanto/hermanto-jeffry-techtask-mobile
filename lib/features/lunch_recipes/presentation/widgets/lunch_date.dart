import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class LunchDate extends StatelessWidget {
  final DateTime date;

  LunchDate(this.date);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 24.0,
        right: 24.0,
        top: 24.0,
      ),
      child: RichText(
        text: TextSpan(
          text: 'Lunch Date: ',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          children: <TextSpan>[
            TextSpan(
              text: '${DateFormat('EEEE, d MMM, yyyy').format(date)}',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.normal,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

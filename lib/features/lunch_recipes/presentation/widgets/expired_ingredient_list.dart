import 'package:flutter/material.dart';

import '../../domain/entities/ingredient.dart';

class ExpiredIngredientList extends StatelessWidget {
  final List<Ingredient> expiredIngredients;

  const ExpiredIngredientList(this.expiredIngredients);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 24.0,
        right: 24.0,
        bottom: 24.0,
      ),
      child: Wrap(
        spacing: 8.0, // gap between adjacent chips
        runSpacing: 0.0, // gap between lines
        children: expiredIngredients.map((ingredient) {
          return Chip(
            label: Text(ingredient.title),
            labelStyle: TextStyle(color: Colors.black),
            backgroundColor: Colors.transparent,
            shape: StadiumBorder(side: BorderSide()),
          );
        }).toList(),
      ),
    );
  }
}

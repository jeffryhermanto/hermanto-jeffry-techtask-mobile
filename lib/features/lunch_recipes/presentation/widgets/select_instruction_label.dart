import 'package:flutter/material.dart';

class SelectInstructionLabel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 24.0,
        left: 24.0,
        right: 24.0,
      ),
      child: Text('Please select these ingredients to get your recipes:'),
    );
  }
}

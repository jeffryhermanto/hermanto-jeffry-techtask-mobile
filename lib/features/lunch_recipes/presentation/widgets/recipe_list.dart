import 'package:flutter/material.dart';

import '../../../../core/util/constants.dart';
import '../../domain/entities/ingredient.dart';
import '../../domain/entities/recipe.dart';

class RecipeList extends StatelessWidget {
  final List<Recipe> recipes;
  final List<Ingredient> selectedIngredients;

  RecipeList({
    @required this.recipes,
    @required this.selectedIngredients,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.all(24.0),
        child: ListView.builder(
            itemCount: recipes.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                padding: EdgeInsets.only(bottom: 15.0),
                child: Card(
                  child: Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text(
                          recipes[index].title,
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Divider(),
                        Wrap(
                          spacing: 8.0, // gap between adjacent chips
                          runSpacing: 0.0, // gap between lines
                          children:
                              recipes[index].ingredients.map((ingredientTitle) {
                            return Chip(
                              label: Text(ingredientTitle),
                              labelStyle: TextStyle(
                                color: selectedIngredients
                                        .where((ingredient) =>
                                            ingredient.title == ingredientTitle)
                                        .toList()
                                        .isEmpty
                                    ? Colors.black
                                    : Colors.white,
                              ),
                              backgroundColor: selectedIngredients
                                      .where((ingredient) =>
                                          ingredient.title == ingredientTitle)
                                      .toList()
                                      .isEmpty
                                  ? Colors.transparent
                                  : kLoadsmileColor,
                              shape: selectedIngredients
                                      .where((ingredient) =>
                                          ingredient.title == ingredientTitle)
                                      .toList()
                                      .isEmpty
                                  ? StadiumBorder(side: BorderSide())
                                  : null,
                            );
                          }).toList(),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/util/constants.dart';
import '../../../../core/widgets/loading_widget.dart';
import '../../../../core/widgets/something_went_wrong.dart';
import '../../domain/entities/ingredient.dart';
import '../bloc/bloc.dart';
import '../widgets/widgets.dart';

class IngredientsScreen extends StatefulWidget {
  static const String id = 'ingredient_screen';

  final DateTime selectedDate;

  IngredientsScreen({@required this.selectedDate});

  @override
  _IngredientsScreenState createState() => _IngredientsScreenState();
}

class _IngredientsScreenState extends State<IngredientsScreen> {
  //* [START Life Cycle]
  @override
  void initState() {
    context.bloc<IngredientsBloc>()..add(LoadIngredients());
    super.initState();
  }
  //* [END Life Cycle]

  //! [START Screen Build]
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: buildBody(context),
    );
  }
  //! [END Screen Build]

  //! [START Extracted Widgets]
  Widget buildAppBar() {
    return AppBar(
      title: Text('Ingredient Selection'),
      centerTitle: true,
      backgroundColor: kLoadsmileColor,
    );
  }

  Widget buildBody(BuildContext context) {
    return BlocBuilder<IngredientsBloc, IngredientsState>(
      builder: (context, state) {
        if (state is IngredientsLoaded) {
          List<Ingredient> ingredients = state.ingredients;
          List<Ingredient> expiredIngredients = ingredients;

          // Filter Ingredients
          ingredients = ingredients.where((ingredient) {
            DateTime parsedUseBy = DateTime.parse(ingredient.useBy);
            return parsedUseBy.isAfter(widget.selectedDate);
          }).toList();

          // Filter Expired Ingredients
          expiredIngredients = expiredIngredients.where((ingredient) {
            DateTime parsedUseBy = DateTime.parse(ingredient.useBy);
            return parsedUseBy.isBefore(widget.selectedDate);
          }).toList();

          // Filter Selected Ingredients
          List<Ingredient> selectedIngredients =
              ingredients.where((ingredient) => ingredient.selected).toList();

          return SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  LunchDate(widget.selectedDate),
                  Divider(
                    indent: 24.0,
                    endIndent: 24.0,
                    thickness: 0.5,
                  ),
                  SelectInstructionLabel(),
                  IngredientList(
                    ingredients: ingredients,
                    onPressed: (ingredient) {
                      setState(() {
                        ingredient.selected = !ingredient.selected;
                        log(ingredient.selected.toString());
                      });
                    },
                  ),
                  ExpiredLabel(),
                  ExpiredIngredientList(expiredIngredients),
                  GetRecipesButton(selectedIngredients),
                ],
              ),
            ),
          );
        } else if (state is IngredientsLoading) {
          return LoadingWidget(text: 'Fetching Ingredients...');
        } else if (state is IngredientsError) {
          return SomethingWentWrong();
        } else {
          return Container();
        }
      },
    );
  }
  //! [END Extracted Widgets]
}

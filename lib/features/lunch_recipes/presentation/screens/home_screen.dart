import 'package:flutter/material.dart';

import '../../../../core/util/constants.dart';
import '../widgets/widgets.dart';
import 'ingredients_screen.dart';

class HomeScreen extends StatefulWidget {
  static const String id = 'home_screen';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  DateTime _date = DateTime.now();

  //? [START Helper Methods]
  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );

    if (picked != null) {
      setState(() => _date = picked);
      _navigateToIngredientsScreen();
    }
  }

  void _navigateToIngredientsScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => IngredientsScreen(selectedDate: _date),
      ),
    ).whenComplete(resetDate);
  }

  void resetDate() {
    setState(() => _date = DateTime.now());
  }
  //? [END Helper Methods]

  //! [START Screen Build]
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: buildBody(context),
    );
  }
  //! [END Screen Build]

  //! [START Extracted Widgets]
  Widget buildAppBar() {
    return AppBar(
      title: Text('Lunch Recipes Suggestion'),
      centerTitle: true,
      backgroundColor: kLoadsmileColor,
    );
  }

  Widget buildBody(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Logo(),
          SizedBox(height: 40.0),
          Text('When do you want to have lunch?'),
          SizedBox(height: 20.0),
          RaisedButton(
            color: kLoadsmileColor,
            child: Text(
              'Select Date',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () => selectDate(context),
          ),
        ],
      ),
    );
  }
  //! [END Extracted Widgets]
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/util/constants.dart';
import '../../../../core/widgets/loading_widget.dart';
import '../../../../core/widgets/something_went_wrong.dart';
import '../../domain/entities/ingredient.dart';
import '../../domain/entities/recipe.dart';
import '../bloc/bloc.dart';
import '../widgets/widgets.dart';

class RecipesScreen extends StatefulWidget {
  static const String id = 'recipes_screen';

  final String apiURL;
  final List<Ingredient> selectedIngredients;

  RecipesScreen(this.apiURL, this.selectedIngredients);

  @override
  _RecipesScreenState createState() => _RecipesScreenState();
}

class _RecipesScreenState extends State<RecipesScreen> {
  //* [START Life Cycle]
  @override
  void initState() {
    context.bloc<RecipesBloc>()..add(LoadRecipes(apiURL: widget.apiURL));
    super.initState();
  }
  //* [END Life Cycle]

  //! [START Screen Build]
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: buildBody(context),
    );
  }
  //! [END Screen Build]

  //! [START Extracted Widgets]
  Widget buildAppBar() {
    return AppBar(
      title: Text('Suggested Recipes'),
      centerTitle: true,
      backgroundColor: kLoadsmileColor,
    );
  }

  Widget buildBody(BuildContext context) {
    return BlocBuilder<RecipesBloc, RecipesState>(
      builder: (context, state) {
        if (state is RecipesLoaded) {
          List<Recipe> recipes = state.recipes;
          List<Recipe> filteredRecipes = [];

          // Filtering Recipes
          for (int i = 0; i < widget.selectedIngredients.length; i++) {
            for (var recipe in recipes) {
              if (recipe.ingredients
                  .contains(widget.selectedIngredients[i].title)) {
                filteredRecipes.add(recipe);
              }
            }
          }

          return filteredRecipes.isEmpty
              ? NoRecipeLabel()
              : RecipeList(
                  recipes: filteredRecipes.toSet().toList(),
                  selectedIngredients: widget.selectedIngredients,
                );
        } else if (state is RecipesLoading) {
          return LoadingWidget(text: 'Loading Recipes...');
        } else if (state is RecipesError) {
          return SomethingWentWrong();
        } else {
          return Container();
        }
      },
    );
  }
  //! [START Extracted Widgets]
}

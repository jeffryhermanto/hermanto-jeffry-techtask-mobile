import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../../core/error/exceptions.dart';
import '../models/recipe_model.dart';

abstract class RecipesRemoteDataSource {
  Future<List<RecipeModel>> getRecipes(String apiURL);
}

class RecipesRemoteDataSourceImpl implements RecipesRemoteDataSource {
  http.Client client;

  RecipesRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<RecipeModel>> getRecipes(String apiURL) async {
    List<RecipeModel> recipes;

    final response = await client.get(
      apiURL,
      headers: {
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      final result = json.decode(response.body) as List<dynamic>;

      recipes = result.map((item) {
        return RecipeModel.fromJson(item);
      }).toList();

      return recipes;
    } else {
      throw ServerException();
    }
  }
}

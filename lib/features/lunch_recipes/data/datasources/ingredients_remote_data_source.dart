import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/util/constants.dart';
import '../models/ingredient_model.dart';

abstract class IngredientsRemoteDataSource {
  Future<List<IngredientModel>> getIngredients();
}

class IngredientsRemoteDataSourceImpl implements IngredientsRemoteDataSource {
  final http.Client client;

  IngredientsRemoteDataSourceImpl({@required this.client});

  @override
  Future<List<IngredientModel>> getIngredients() async {
    List<IngredientModel> ingredients;

    String apiURL = kMockBaseURL + '/ingredients';

    final response = await client.get(
      apiURL,
      headers: {
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      final result = json.decode(response.body) as List<dynamic>;

      ingredients = result.map((item) {
        return IngredientModel.fromJson(item);
      }).toList();

      return ingredients;
    } else {
      throw ServerException();
    }
  }
}

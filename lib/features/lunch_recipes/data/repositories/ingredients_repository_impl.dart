import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/ingredient.dart';
import '../../domain/repositories/ingredients_repository.dart';
import '../datasources/ingredients_remote_data_source.dart';

class IngredientsRepositoryImpl implements IngredientsRepository {
  final IngredientsRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  IngredientsRepositoryImpl({
    @required this.remoteDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<Ingredient>>> getIngredients() async {
    if (await networkInfo.isConnected) {
      try {
        List<Ingredient> ingredients = await remoteDataSource.getIngredients();
        return Right(ingredients);
      } on PlatformException catch (e) {
        return Left(ServerFailure(message: e.message));
      }
    } else {
      return Left(ServerFailure(message: 'No Network'));
    }
  }
}

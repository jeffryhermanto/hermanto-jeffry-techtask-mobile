import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/recipe.dart';
import '../../domain/repositories/recipes_repository.dart';
import '../datasources/recipes_remote_data_source.dart';

class RecipesRepositoryImpl implements RecipesRepository {
  final RecipesRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  RecipesRepositoryImpl({
    @required this.remoteDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<Recipe>>> getRecipes(String apiURL) async {
    if (await networkInfo.isConnected) {
      try {
        List<Recipe> recipes = await remoteDataSource.getRecipes(apiURL);
        return Right(recipes);
      } on PlatformException catch (e) {
        return Left(ServerFailure(message: e.message));
      }
    } else {
      return Left(ServerFailure(message: 'No Network'));
    }
  }
}

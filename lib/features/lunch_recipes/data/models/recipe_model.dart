import 'package:meta/meta.dart';

import '../../domain/entities/recipe.dart';

class RecipeModel extends Recipe {
  RecipeModel({
    @required final String title,
    @required final List ingredients,
  }) : super(
          title: title,
          ingredients: ingredients,
        );

  factory RecipeModel.fromJson(Map<String, dynamic> object) {
    return RecipeModel(
      title: object['title'],
      ingredients: object['ingredients'],
    );
  }
}

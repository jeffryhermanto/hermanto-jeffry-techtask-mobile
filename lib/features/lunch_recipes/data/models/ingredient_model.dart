import 'package:meta/meta.dart';

import '../../domain/entities/ingredient.dart';

class IngredientModel extends Ingredient {
  IngredientModel({
    @required final String title,
    @required final String useBy,
  }) : super(
          title: title,
          useBy: useBy,
        );

  factory IngredientModel.fromJson(Map<String, dynamic> object) {
    return IngredientModel(
      title: object['title'],
      useBy: object['use-by'],
    );
  }
}

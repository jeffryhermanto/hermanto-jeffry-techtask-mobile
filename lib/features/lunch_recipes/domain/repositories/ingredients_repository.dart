import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../entities/ingredient.dart';

abstract class IngredientsRepository {
  Future<Either<Failure, List<Ingredient>>> getIngredients();
}

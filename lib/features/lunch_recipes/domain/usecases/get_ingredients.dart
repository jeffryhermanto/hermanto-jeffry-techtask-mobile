import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../entities/ingredient.dart';
import '../repositories/ingredients_repository.dart';

class GetIngredients implements UseCase<List<Ingredient>, NoParams> {
  final IngredientsRepository repository;

  GetIngredients(this.repository);

  @override
  Future<Either<Failure, List<Ingredient>>> call(NoParams params) async {
    return await repository.getIngredients();
  }
}

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../entities/recipe.dart';
import '../repositories/recipes_repository.dart';

class GetRecipes implements UseCase<List<Recipe>, RecipeParams> {
  final RecipesRepository repository;

  GetRecipes(this.repository);

  @override
  Future<Either<Failure, List<Recipe>>> call(RecipeParams params) async {
    return await repository.getRecipes(params.apiURL);
  }
}

class RecipeParams extends Equatable {
  final String apiURL;

  RecipeParams({@required this.apiURL});

  @override
  List<Object> get props => [apiURL];
}

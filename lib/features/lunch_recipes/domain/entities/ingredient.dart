import 'package:meta/meta.dart';

import '../../data/models/ingredient_model.dart';

class Ingredient {
  final String title;
  final String useBy;
  bool selected = false;

  Ingredient({
    @required this.title,
    @required this.useBy,
  });

  IngredientModel toModel() {
    return IngredientModel(
      title: title,
      useBy: useBy,
    );
  }
}

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../data/models/recipe_model.dart';

class Recipe extends Equatable {
  final String title;
  final List ingredients;

  Recipe({
    @required this.title,
    @required this.ingredients,
  });

  @override
  List<Object> get props => [title, ingredients];

  RecipeModel toModel() {
    return RecipeModel(
      title: title,
      ingredients: ingredients,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'features/lunch_recipes/presentation/bloc/bloc.dart';
import 'features/lunch_recipes/presentation/screens/home_screen.dart';
import 'injection_container.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<IngredientsBloc>(
          create: (_) => sl<IngredientsBloc>(),
        ),
        BlocProvider<RecipesBloc>(
          create: (_) => sl<RecipesBloc>(),
        ),
      ],
      child: MaterialApp(
        title: 'Lunch Recipes Suggestion',
        initialRoute: HomeScreen.id,
        routes: {
          HomeScreen.id: (context) => HomeScreen(),
        },
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
